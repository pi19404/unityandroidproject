package com.example.androidunityinterface;

import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.unity3d.player.UnityPlayerNativeActivity;
import android.view.ViewGroup.LayoutParams;

public class MainActivity extends UnityPlayerNativeActivity  {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final long delay = 1000;//ms
        Handler handler = new Handler();

        Runnable runnable = new Runnable() {
            public void run() {
                ViewGroup rootView = (ViewGroup) MainActivity.this.findViewById(android.R.id.content);
                rootView.setKeepScreenOn(true);

                //get the topmost view
                View topMostView = getLeafView(rootView);
                // let's add a sibling to the leaf view
                ViewGroup leafParent = (ViewGroup)topMostView.getParent();

                //inflate the android view to be added
                View view = getLayoutInflater().inflate(R.layout.activity_main, null, false);
                view.setKeepScreenOn(true);

                //add the android view on top of unity view
                leafParent.addView(view, new LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.FILL_PARENT));
                Log.e("TTTT","added the leaf view");
            }
        };

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        handler.postDelayed(runnable, delay);

    }


    private View getLeafView(View view) {
        if (view instanceof ViewGroup) {
            ViewGroup vg = (ViewGroup)view;
            for (int i = 0; i < vg.getChildCount(); ++i) {
                View chview = vg.getChildAt(i);
                View result = getLeafView(chview);
                if (result != null)
                    return result;
            }
            return null;
        }
        else {

            Log.e("ZZ","Found leaf view");
            return view;
        }
    }

}
